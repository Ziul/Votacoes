from flask import Flask, request, redirect, send_from_directory
from os import system, rmdir
from shutil import rmtree
from subprocess import run

app = Flask(__name__)


@app.route('/')
def home():
    return '''
<html>
  <head>
    <title>Home Page</title>
  </head>
  <body>
    <h1>Baixe os dados das votações</h1>

    <form action="/action">
    Camara: <input type="text" name="camara" value="http://"><br>
    Estadão: <input type="text" name="estadao" value="http://"><br>
    <input type="submit" value="Submit">
    </form>

  </body>
</html>
'''


@app.route('/output/<path:filename>', methods=['GET', 'POST'])
def download_file(filename):
    return send_from_directory(directory='./output', filename=filename)


@app.route('/action')
def action():
    entrada = request.args
    try:
        system('rm -rf output/*.csv')
    except Exception as e:
        rmtree('output')

    get_camara(entrada.get('camara'))
    get_estadao(entrada.get('estadao'))

    saida = """
<html>
  <head>
    <title>Home Page</title>
  </head>
  <body>
    <h1>Arquivos gerados</h1>
"""
    if len(entrada.get('camara')) > 10:
        saida += '<br> <a href="/output/camara.csv">Camara</a> '
    if len(entrada.get('estadao')) > 10:
        saida += '<br> <a href="/output/estadao.csv">Estadão</a> '

    saida += """

    <form action="/">
    <input type="submit" value="Retornar">
    </form>

  </body>
</html>
"""
    return saida


def get_camara(url):
    if len(url) < 10:
        return
    cmd = 'scrapy runspider camara.py -t csv -o output/camara.csv -a start_url={}'.format(
        url)

    return run(cmd.split(' '), timeout=30)


def get_estadao(url):
    if len(url) < 10:
        return
    cmd = 'scrapy runspider estadao.py -t csv -o output/estadao.csv -a start_url={}'.format(
        url)

    return run(cmd.split(' '), timeout=30)

if __name__ == '__main__':
    app.run(debug=True)
