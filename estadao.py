import scrapy


class deputadoItem(scrapy.Item):
    nome = scrapy.Field()
    role = scrapy.Field()
    partido = scrapy.Field()
    estado = scrapy.Field()
    voto = scrapy.Field()


class ParseVotos(scrapy.Spider):
    """docstring for ParseVotos"""
    name = 'Previdencia'
    # start_urls = [
    #     'http://infograficos.estadao.com.br/especiais/placar/votacao/economia/?id=GLwN7vXR3W']

    def __init__(self, *args, **kwargs):
        super(ParseVotos, self).__init__(*args, **kwargs)

        self.start_urls = [kwargs.get('start_url')]

    def parse(self, response):

        sections = response.xpath('//*[@class="voters list"]/section')
        votos_necessário = response.css(
            '.threshold > p:nth-child(1)').xpath('output/text()').extract_first()

        for section in sections:
            voto = section.xpath('header/h3/text()').extract_first()

            deputados_response = section.css('div.h-card.item')
            for deputado_response in deputados_response:
                deputado = {}
                deputado['nome'] = deputado_response.css(
                    'span.p-name::text').extract_first()
                deputado['role'] = deputado_response.css(
                    'span.p-role::text').extract_first()
                deputado['partido'] = deputado_response.css(
                    'span.p-org::text').extract_first()
                deputado['estado'] = deputado_response.css(
                    'span.p-region::text').extract_first()
                deputado['voto'] = voto

                yield deputado

        yield {'nome': response.url}
