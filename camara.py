import scrapy


def cleaner(durty_text):
    return durty_text.replace('\r', '').replace('\n', '').replace('\t', '').replace('  ', '').replace('\xa0', '')


class ParseVotos(scrapy.Spider):
    """docstring for ParseVotos"""
    name = 'Camara'
    # start_urls = [
    #     'http://www.camara.leg.br/internet/votacao/mostraVotacao.asp?ideVotacao=7477&numLegislatura=55&codCasa=1&numSessaoLegislativa=3&indTipoSessaoLegislativa=O&numSessao=83&indTipoSessao=E&tipo=uf'
    #     ]

    def __init__(self, *args, **kwargs):
        super(ParseVotos, self).__init__(*args, **kwargs)

        self.start_urls = [kwargs.get('start_url')]

    def parse(self, response):
        pass
        resultado, orientacoes, estados = response.css('table.tabela-2')

        # Tabela de orientação
        orientacoes_parsed = {}
        for partido, orientacao in zip(orientacoes.css('th'), orientacoes.css('td')):
            orientacoes_parsed[partido.css('::text').extract_first().replace(
                ' ', '').replace(':', '')] = cleaner(orientacao.css('::text').extract_first())
            # 'partido': partido.css('::text').extract_first(),
            # 'orientação': orientacao.css('::text').extract_first()

        print(orientacoes_parsed)
        # tabela de estados
        estado = ''
        for linha in estados.css('tbody > tr '):
            try:
                if linha.css('th') != []:
                    estado = cleaner(linha.css('th::text').extract_first())
                else:
                    parsed = linha.css('td::text').extract()
                    yield {
                        'parlamentar': cleaner(parsed[0]),
                        'partido': cleaner(parsed[1]),
                        'bloco': cleaner(parsed[2]),
                        'voto': cleaner(parsed[3]),
                        'orientação': orientacoes_parsed.get(
                            cleaner(parsed[1]).replace(' ', ''),
                            orientacoes_parsed.get(
                                cleaner(parsed[2]).replace(' ', ''), '')
                        ),
                        'estado': estado,
                    }
            except Exception as e:
                pass

        yield {'parlamentar': str(response.url)}
