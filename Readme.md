# Crawler para ComprasNet


### Requisitos

- [Python 3](https://www.python.org/downloads/)
- [Pip](https://pypi.python.org/pypi/pip)
- Internet

### Instalando dependências

No raiz do projeto, onde se encontra o arquivo `requeriments.txt` execute o seguinte comando:

	# pip install -r requeriments.txt

### Rodando a aplicação

Considerando estar do diretório onde o arquivo `estadao.py` se encontra, basta  entrar com o seguinte comando:

	$ scrapy runspider estadao.py

Para criar um arquivo CSV como saída do *crawler*, execute-o com as seguintes *flags*:

	$ scrapy runspider estadao.py -t csv -o arquivo_de_saida.csv

Ao final da execução, o *crawler* enviará o arquivo para os emails registrados no inicio do arquivo. 
Caso um arquivo de saída não seja denominado ou a lista de emails de destino esteja vazia, não será enviado nenhum email.

